extends "State.gd"

const TURN_RATE = 2
var turn_timer = 0

func _on_enter_state():
    target.get_node("Sprite").animation = "female_idle"
    turn_timer = TURN_RATE
    
func _process(delta):
    turn_timer -= delta
    if turn_timer <= 0:
        turn()
        _on_enter_state()
       
func turn():
    target.sprite.flip_h = !target.sprite.flip_h