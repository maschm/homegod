extends "State.gd"

export var WALK_TIME = 0.01

var path_vertex_index = 0
var index_direction = 1
var walk_start_pos = Vector2()
var walk_target = Vector2()
var cur_walk_time = WALK_TIME

var timer = 0

func _on_enter_state():
    var nearest = util.find_nearest_line_point(target.house.path, target.position)
    path_vertex_index = nearest.index
    walk_start_pos = target.position
    walk_target = nearest.point
    cur_walk_time = (walk_start_pos - walk_target).length() * WALK_TIME
    timer = cur_walk_time
    update_animation()  
    
func next_point():
    path_vertex_index = (path_vertex_index + index_direction)
    if path_vertex_index >= target.house.path.get_point_count():
        path_vertex_index = target.house.path.get_point_count() - 2
        index_direction = -1
    elif path_vertex_index < 0:
        path_vertex_index = 1
        index_direction = 1
    walk_start_pos = target.position
    walk_target = target.house.path.get_point_position(path_vertex_index)
    cur_walk_time = (walk_start_pos - walk_target).length() * WALK_TIME
    timer = cur_walk_time
    update_animation()

func update_animation():
    var sprite = target.get_node("Sprite")
    sprite.animation = "female_walk"
    sprite.flip_h = walk_target.x > walk_start_pos.x
    
func _process(delta):
    target.position = walk_start_pos.linear_interpolate(walk_target, 1 - timer / cur_walk_time)

    timer -= delta
    if timer <= 0:
        next_point()