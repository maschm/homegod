extends "State.gd"

const Furniture = preload("res://furniture/Furniture.gd")

const TURN_RATE = 2
var turn_timer

func _on_enter_state():
    turn_timer = TURN_RATE
    var wish_furniture_id = util.rand_from_array(target.house.wanted_furniture_ids)
    var wish_furniture = Furniture.FURNITURE[wish_furniture_id]
    target.get_node("Thought").visible = true
    target.get_node("Thought/Sprite").frame = wish_furniture.icon
    var sprite = target.get_node("Sprite")
    sprite.animation = "female_idle"
    
func _on_exit_state():
    target.get_node("Thought").visible = false
    
func _process(delta):
    turn_timer -= delta
    if turn_timer <= 0:
        _on_enter_state()