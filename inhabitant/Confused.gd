extends "State.gd"

func _on_enter_state():
    target.wonder.emitting = true
    target.sprite.animation = "female_idle"
    
func _on_exit_state():
    target.wonder.emitting = false

func _process(delta):
    pass
