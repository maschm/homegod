extends Node2D

const Idle = preload("Idle.gd")
const Walk = preload("Walk.gd")
const Wish = preload("Wish.gd")
const React = preload("React.gd")
const Confused = preload("Confused.gd")

var current_state
var state_timer = 0

export var WALK_CHANCE = .4
export var WISH_CHANCE = .4
export var IDLE_TIME = 5
export var WALK_TIME = 12
export var WISH_TIME = 2
export var REACT_TIME = 10

onready var house = get_parent()
onready var reaction = $Reaction
onready var wonder = $WTF
onready var sprite = $Sprite

var IdleState = Idle.new()
var WalkState = Walk.new()
var WishState = Wish.new()
var ReactState = React.new()
var ConfusedState = Confused.new()

func _ready():
    current_state = IdleState
    current_state.set_target(self)
    $Sprite.playing = true

func next_state():
    var rand = randf()
    if rand < WALK_CHANCE:
        set_state(WalkState)
        state_timer = WALK_TIME + rand_range(-4, 4)
    elif rand < WISH_CHANCE + WALK_CHANCE && house.wanted_furniture_ids.size() > 0:
        set_state(WishState)
        state_timer = WISH_TIME + rand_range(-1, 1)
    else:
        set_state(IdleState)
        state_timer = IDLE_TIME + rand_range(-2, 2)

func set_state(state):
    if current_state.has_method("_on_exit_state"): current_state._on_exit_state()
    current_state = state
    current_state.set_target(self)
    if current_state.has_method("_on_enter_state"): current_state._on_enter_state()

func _process(delta):
    current_state._process(delta)
    state_timer -= delta
    if state_timer < 0:
        next_state()
        
func react_to_wrong_furniture():
    set_state(ConfusedState)
    state_timer = 7.0 + rand_range(-1, 3)

func react_to_new_furniture(furniture_point):
    ReactState.walk_target = furniture_point.position
    set_state(ReactState)
    state_timer = REACT_TIME + rand_range(-5, 5)