extends "State.gd"

export var WALK_TIME = 0.01

var walk_start_pos = Vector2()
var walk_target = Vector2()
var cur_walk_time = WALK_TIME
var furniture_pos = Vector2()
var timer = 0

func _on_enter_state():
    furniture_pos = walk_target
    var nearest = util.find_nearest_line_point(target.house.path, walk_target)
    walk_start_pos = target.position
    walk_target = nearest.point
    target.sprite.flip_h = walk_target.x > walk_start_pos.x
    cur_walk_time = (walk_start_pos - walk_target).length() * WALK_TIME
    timer = cur_walk_time
    update_animation()
    
func _on_exit_state():
    if target.house.wanted_furniture_ids.size() > 0:
        target.reaction.emitting = false
        
func update_animation():
    target.sprite.playing = true
    target.sprite.animation = "female_walk"
    target.sprite.flip_h = walk_target.x > walk_start_pos.x

func _process(delta):
    target.position = walk_start_pos.linear_interpolate(walk_target, 1 - timer / cur_walk_time)

    if timer > 0:
        timer -= delta
    if timer <= 0:
        target.sprite.flip_h = furniture_pos.x > target.position.x
        target.sprite.animation = "female_idle"
        target.reaction.emitting = true