extends Node2D

onready var world_node = get_node("/root/World")

var score_req

func _ready():
    while true:
        yield(world_node, "mission_completed")
        if world_node.score >= score_req:
            queue_free()
            return
            
    