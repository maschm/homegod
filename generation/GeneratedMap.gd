extends Node2D

var main_noise
var horizon_noise
var bedrock_noise

const ROOM_SIZE = Vector2(13,9)
const CHUNK_ROOMS = Vector2(3,2)
const CHUNK_SIZE = ROOM_SIZE * CHUNK_ROOMS

var RENDER_DISTANCE = 2
var HORIZON_VARIANCE = 8
const HORIZON_OFFSET = 4
const BEDROCK_WIDTH = 10
const TERRAIN_THRESHOLD = 0.4
const DOOR_RADIUS = 1
    
var generated_chunks = {}

onready var ROOM_MASK = generate_room_mask()

export(NodePath) var player_path
var player

const UP = Vector3(0,-1,   1) # last value represents direction flag
const RIGHT = Vector3(1,0,   2)
const DOWN = Vector3(0,1,   4)
const LEFT = Vector3(-1,0,   8)
const DIRS = [UP, RIGHT, DOWN, LEFT]

func _ready():
    
    # get player if set in property editor
    if player_path != null:
        player = get_node(player_path)

    # reset    
    generated_chunks = {}
    $layer_0.clear()
    
    randomize()
    # init noise gen
    main_noise = OpenSimplexNoise.new()
    main_noise.seed = randi()
    main_noise.octaves = 2
    main_noise.period = 15.0
    main_noise.persistence = 0.8
    
    # init noise gen
    horizon_noise = OpenSimplexNoise.new()
    horizon_noise.seed = randi()
    horizon_noise.octaves = 2
    horizon_noise.period = 20.0
    horizon_noise.persistence = 0.8
    
    # init noise gen
    bedrock_noise = OpenSimplexNoise.new()
    bedrock_noise.seed = randi()
    bedrock_noise.octaves = 4
    bedrock_noise.period = 20.0
    bedrock_noise.persistence = 0.8
    
    # Clone tileset
    $layer_0.tile_set = $TileSheetDummy_0.get_node("Tile Layer 1").tile_set
    $layer_gen.tile_set = $TileSheetDummy_gen.get_node("Tile Layer 1").tile_set

func _process(delta):
    var player_x = 0
    if player != null:
        player_x = x_to_chunk_x(player.position.x / scale.x)
    else:
        RENDER_DISTANCE = 3
    
    for x in range(-RENDER_DISTANCE, RENDER_DISTANCE+1):
        var chunk_x = player_x+x
        if !is_chunk_generated(chunk_x):
            generate_chunk(chunk_x)

func x_to_chunk_x(x):
    return int(floor(-0.5 + x / 64 / CHUNK_SIZE.x))

func chunk_x_to_id(chunk_x):
    return String(chunk_x)

func is_chunk_generated(chunk_x):
    return generated_chunks.has(chunk_x_to_id(chunk_x))
    
func set_chunk_as_generated(chunk_x):
    generated_chunks[chunk_x_to_id(chunk_x)] = true


func generate_room_mask():
    
    # First, create empty 0-matrix
    var arr = []
    for y in range(ROOM_SIZE.y):
        arr.append([])
        for x in range(ROOM_SIZE.x):
            arr[y].append(0)
    # Add Borders
    for y in range(ROOM_SIZE.y):
        arr[y][0] = 1
        arr[y][ROOM_SIZE.x-1] = 1
    for x in range(ROOM_SIZE.x):
        arr[0][x] = 1
        arr[ROOM_SIZE.y-1][x] = 1
    return arr

func generate_chunk(chunk_x):
    set_chunk_as_generated(chunk_x)
    
    # Create empty chunk
    var arr = []
    for y in range(CHUNK_SIZE.y):
        arr.append([])
        for x in range(CHUNK_SIZE.x):
            arr[y].append(0)
          
    
    # Add room masks
    for ry in range(CHUNK_ROOMS.y):
        for rx in range(CHUNK_ROOMS.x):
            for y in range(ROOM_SIZE.y):
                for x in range(ROOM_SIZE.x):
                    arr[ry * ROOM_SIZE.y + y][rx * ROOM_SIZE.x + x] = ROOM_MASK[y][x]


    # Add doors to mask
    var doormasks = snake()      
    for ry in range(CHUNK_ROOMS.y):
        for rx in range(CHUNK_ROOMS.x):
            var doormask = doormasks[ry][rx]
            for dir in DIRS:
                if (doormask & int(round(dir.z))) != 0:
                    arr = add_door(arr, rx, ry, dir)
            
    arr = blur_matrix(arr)
    
    # Convert arr to TileMap
    for x in range(CHUNK_SIZE.x):
        var tx = chunk_x * CHUNK_SIZE.x + x
        
        
        for y in range(CHUNK_SIZE.y):
            var ty = y
            # go to next column if reached horizon
            #if ty < horizon:
            #    break
            var noise = main_noise.get_noise_2d(tx, ty) 
            var mask = arr[y][x]
            if 3*noise + 2*mask > 0.4: # or ty > bedrock 
                $layer_gen.set_cell(tx, ty, 1)
        
        # add top horizon
        var horizon = HORIZON_VARIANCE * horizon_noise.get_noise_2d(tx, 0) + HORIZON_OFFSET
        if $layer_0.get_cell(tx, 0) == 2:
            for y in range(1, -horizon-1, -1):
                $layer_gen.set_cell(tx, y, 1)
        # add bedrock
        for y in range(CHUNK_SIZE.y, CHUNK_SIZE.y+BEDROCK_WIDTH, 1):
            $layer_gen.set_cell(tx, y, 1)
            
        for y in range(-HORIZON_VARIANCE-HORIZON_OFFSET-1, CHUNK_SIZE.y+BEDROCK_WIDTH):
            for x in range(-1, CHUNK_SIZE.x+1):
                update_tile(chunk_x * CHUNK_SIZE.x + x, y)
                

func update_tile(x,y):
    var tl = int($layer_gen.get_cell(x-1,y-1)==1)
    var t = int($layer_gen.get_cell(x,y-1)==1)
    var tr = int($layer_gen.get_cell(x+1,y-1)==1)
    var r = int($layer_gen.get_cell(x+1,y)==1)
    var br = int($layer_gen.get_cell(x+1,y+1)==1)
    var b = int($layer_gen.get_cell(x,1+y)==1)
    var bl = int($layer_gen.get_cell(x-1,y+1)==1)
    var l = int($layer_gen.get_cell(x-1,y)==1)
    var mask_tl = 1*tl | 2*t | 4*l
    var mask_tr = 1*tr | 2*t | 4*r
    var mask_bl = 1*bl | 2*b | 4*l
    var mask_br = 1*br | 2*b | 4*r
    
    #                          0    1   2   3   4   5   6   7
    var tile_id_by_mask_tl = [67,  67,  1,  1, 23, 23, 61, 42]
    var tile_id_by_mask_tr = [26,  26,  2,  2, 24, 24, 22, 43]
    var tile_id_by_mask_bl = [123, 87, 21, 21, 81, 81, 83, 62]
    var tile_id_by_mask_br = [124,124, 64, 64, 82, 82, 44, 63]
    
    if $layer_gen.get_cell(x,y) == 1:
        $layer_0.set_cell(2*x,2*y-1,tile_id_by_mask_tl[mask_tl]+1)
        $layer_0.set_cell(2*x+1,2*y-1,tile_id_by_mask_tr[mask_tr]+1)
        $layer_0.set_cell(2*x,2*y,tile_id_by_mask_bl[mask_bl]+1)
        $layer_0.set_cell(2*x+1,2*y,tile_id_by_mask_br[mask_br]+1)
        #$layer_0.set_cell(2*x+1,2*y,tile_id_by_mask_bl[mask]+1)
        #$layer_0.set_cell(2*x+1,2*y+1,tile_id_by_mask_br[mask]+1)
        
    pass
            

func snake():
    # set up initial vars
    var initial = Vector2(randi()%int(round(CHUNK_ROOMS.x)), 0)
    
    
    # init data structures
    var stack = [initial]      # stack of snake
    var result = []            # contains one or multiple doorflags per room
    var has_visited = []
    
    # Create empty result
    for y in range(CHUNK_ROOMS.y):
        result.append([])
        has_visited.append([])
        for x in range(CHUNK_ROOMS.x):
            result[y].append(0)
            has_visited[y].append(0)
            
    # set up initial vars
    result[initial.y][initial.x] = int(round(UP.z))
    has_visited[initial.y][initial.x] = true
    
    var counter = 0
    while counter + 1 < CHUNK_ROOMS.x * CHUNK_ROOMS.y:
        var now = stack[0]

        # check which directions are ok ans save them as possibilities
        var possibilities = []
        for d in DIRS:
            var maybe = Vector2(now.x+d.x, now.y+d.y)
            if maybe.x < 0 || maybe.x >= CHUNK_ROOMS.x:
                continue
            if maybe.y < 0 || maybe.y >= CHUNK_ROOMS.y:
                continue
            if has_visited[maybe.y][maybe.x]:
                continue
            possibilities.push_back(d)
        
        # backtrack if no way left
        if possibilities.size() == 0:
            stack.pop_front()
            continue

        # select directions
        var selected_id = randi()%possibilities.size();
        var selected_possibility = possibilities[selected_id]
        
        var opposite_dir;
        match selected_possibility.z:
            UP.z: opposite_dir = DOWN
            RIGHT.z: opposite_dir = LEFT
            DOWN.z: opposite_dir = UP
            LEFT.z: opposite_dir = RIGHT

        
        # save to data structures
        var target = Vector2(now.x+selected_possibility.x, now.y+selected_possibility.y)
        result[target.y][target.x] |= int(round(opposite_dir.z))
        result[now.y][now.x] |= int(round(selected_possibility.z))
        stack.push_front(target)
        has_visited[target.y][target.x] = true
        counter+=1
        
        
    return result
    
func add_door(mask, rx, ry, dir):
    var hr = ROOM_SIZE.x/2.0;
    var target_pt = Vector2((1 + dir.x) * hr, (1 + dir.y) * hr)
    var start_pt;
    var end_pt;
    match dir.z:
        UP.z:
            start_pt = target_pt - Vector2(DOOR_RADIUS,0)
            end_pt = target_pt + Vector2(DOOR_RADIUS,hr)
        RIGHT.z:
            start_pt = target_pt - Vector2(hr,DOOR_RADIUS)
            end_pt = target_pt + Vector2(0,DOOR_RADIUS)
        DOWN.z:
            start_pt = target_pt - Vector2(DOOR_RADIUS,hr)
            end_pt = target_pt + Vector2(DOOR_RADIUS,0)
        LEFT.z:
            start_pt = target_pt - Vector2(0,DOOR_RADIUS)
            end_pt = target_pt + Vector2(hr,DOOR_RADIUS)
    
    for y in range(start_pt.y, end_pt.y+1):
        for x in range(start_pt.x, end_pt.x+1):
            mask[ry * ROOM_SIZE.y + y][rx * ROOM_SIZE.x + x] -= 2
    
    return mask

func blur_matrix(m):
    
    var w = 1
    var b = []
    
    var sum = 0
    for x in range(-w,w+1):
        var y = exp(-pow(x,2)/(2*4*4))
        sum += y
        b.append(y)
    pass
    for x in range(-w,w+1):
        b[x+w] /= sum
    pass
    
    var new_m = []
    for y in range(m.size()):
        new_m.append([])
        for x in range(m[0].size()):
            new_m[y].append(0)
            for ix in range(-w,w+1):
                var ex = clamp(x+ix, 0, m[0].size()-1)
                new_m[y][x] += b[ix]*m[y][ex]
    
    m = new_m
    new_m = []
    
    for y in range(m.size()):
        new_m.append([])
        for x in range(m[0].size()):
            new_m[y].append(0)
            for iy in range(-w,w+1):
                var ey = clamp(y+iy, 0, m.size()-1)
                new_m[y][x] += b[iy]*m[ey][x]
                
    return new_m
    
    