tool
extends RigidBody2D

enum Type {GROUND, WALL, CEILLING}

const FURNITURE = [
    {"name": "microwave",     #0
        "type": Type.GROUND,
        "sprite": 110,
        "icon": 30 },
    {"name": "stove",      #1
        "type": Type.GROUND,
        "sprite": 111,
        "icon": 30 },
    {"name": "chouch_chair",      #2
        "type": Type.GROUND,
        "sprite": 112,
        "icon": 31 },
    {"name": "lamp",      #3
        "type": Type.GROUND,
        "sprite": 113,
        "icon": 32 },
    {"name": "toilett",      #4
        "type": Type.GROUND,
        "sprite": 114,
        "icon": 33 },
    {"name": "picture_k",      #5
        "type": Type.WALL,
        "sprite": 115,
        "icon": 34 },
    {"name": "picture_ggj",      #6
        "type": Type.WALL,
        "sprite": 116,
        "icon": 34 },
    {"name": "picture_house",      #7
        "type": Type.WALL,
        "sprite": 117,
        "icon": 34 },
    {"name": "chandelier",      #8
        "type": Type.CEILLING,
        "sprite": 118,
        "icon": 32 },
    {"name": "fireplace",      #9
        "type": Type.GROUND,
        "sprite": 119,
        "icon": 35 },
    {"name": "radiator",     #10
        "type": Type.GROUND,
        "sprite": 120,
        "icon": 35 },
    {"name": "bin",     #11
        "type": Type.GROUND,
        "sprite": 121,
        "icon": 33 },
    {"name": "lightbulb",     #12
        "type": Type.CEILLING,
        "sprite": 122,
        "icon": 32 }]
        

const MIN_FOLLOW_SPEED_DISTANCE = 160
const MIN_FOLLOW_SPEED = 0
const MAX_FOLLOW_SPEED_DISTANCE = 400
const MAX_FOLLOW_SPEED = 300

const ROTATION_DURATION = 0.4

# position relative to Player 0,0
export var furniture_id = -1;
export(Type) var type = Type.GROUND

var target_position = null
var floating = false
var time = 0.0

func _ready():
    if self.furniture_id != -1:
        init(furniture_id);
    
func init(furniture_id):
    var fun = FURNITURE[furniture_id]
    $Sprite.frame = fun.sprite
    self.furniture_id = furniture_id
    self.type = fun.type
    
func _process(delta):    
    if !target_position:
        return
    
    time += delta
    
    var move = target_position - global_position
    var length = move.length()
            
    if length <= 0.0:
        if !floating:
            set_target_position(null, false)
        return

    var ramp = clamp((length - MIN_FOLLOW_SPEED_DISTANCE) / (MAX_FOLLOW_SPEED_DISTANCE - MIN_FOLLOW_SPEED_DISTANCE), 0.0, 1.0)

    var speed = ramp*(MAX_FOLLOW_SPEED-MIN_FOLLOW_SPEED)+MIN_FOLLOW_SPEED;
    if !floating:
        speed = MAX_FOLLOW_SPEED
        
    var step_speed = min(length, speed*delta)
    if length > step_speed:
        move = move * (step_speed / length);
    position += move
    

func set_target_position(p, floating):
    if (target_position == null) != (p == null):
        var enabled = p != null
        $OrangeParticles.emitting = enabled
        $BlueParticles.emitting = enabled
        $Floating.playing = enabled
        
    self.floating = floating
    target_position = p
    
    $Tween.interpolate_property(self, "rotation_degrees", rotation_degrees, 0, ROTATION_DURATION, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
    $Tween.start()


func is_controlled():
    return target_position != null

func place_in_house():
    $Pickup/CollisionShape2D.disabled = true
    remove_child($Pickup)
    mode = MODE_STATIC
    self.collision_mask = 0
    self.collision_layer = 0
    self.z_index = 0