extends Node2D

onready var barrier_scene = preload("res://barrier/Barrier.tscn")

func _ready():
    var barrier_layers = $map/Barriers.get_children()
    
    for barrier_layer in barrier_layers:
        var score_req = int(barrier_layer.get_name())
        var cells = barrier_layer.get_used_cells()
        
        for cell in cells:
            barrier_layer.set_cell(cell.x, cell.y, 0)
            var barrier_node = barrier_scene.instance()
            barrier_node.position = Vector2((cell.x+0.5)*64, (cell.y+0.5)*64)
            barrier_node.score_req = score_req
            add_child(barrier_node)
            