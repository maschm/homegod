extends Node

signal mission_completed
signal level_completed

var score = 0
var level = 0

func mission_completed():
    score += 1
    level += 1
    emit_signal("mission_completed")

func level_completed():
    emit_signal("level_completed")