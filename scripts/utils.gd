extends Node

static func iff(c, i, e):
    if c:     return i;
    else:     return e;

static func rands():
    return iff(randi()%2==0, 1, -1)

static func remove_first_from_array(arr: Array, item):
    var pos = arr.find(item)
    if pos >= 0:
        arr.remove(pos)

static func rand_from_array(arr):
    return arr[randi() % arr.size()]

static func find_nearest_line_point(line: Line2D, pos: Vector2):
    var min_dist = 100000
    var min_point = Vector2()
    var min_index = -1
    for i in range(line.get_point_count()):
        var point = line.get_point_position(i)
        var dist = (pos - point).length()
        if dist < min_dist:
            min_dist = dist
            min_point = point
            min_index = i
    return { "point": min_point, "index": min_index }
