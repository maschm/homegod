extends Control

onready var world_node = get_node("/root/World")
onready var spawns_node = get_node("/root/World/Map/map/Spawns")
var furniture_scene = preload("res://furniture/Furniture.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
    randomize()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    pass


func generate_missions():
    while(true):
        var wanted_furniture = get_wanted_furniture()
        if wanted_furniture == -1:
            world_node.level_completed()
            return
        spawn_furniture(wanted_furniture, get_spawn_position())
        show_message_new_mission(wanted_furniture)
    
        yield(world_node, "mission_completed")
        show_message_finished_mission()
        yield($AnimationPlayer, "animation_finished")
        

func show_message_new_mission(objective):
    $HBoxContainer/GodTexture/Speech/Icon.frame = 110 + objective
    $AnimationPlayer.play("MissionStart")
    
func show_message_finished_mission():
    $AnimationPlayer.play("MissionSuccess")

func _on_FurnitureSpawnTimer_timeout():
    generate_missions()
    
func spawn_furniture(id, pos):
    if id == -1 || pos == null:
        return
        
    var furniture = furniture_scene.instance()
    furniture.init(id)
    world_node.add_child(furniture)
    furniture.global_position = pos
    
func get_wanted_furniture():
    var house_nodes = world_node.find_node("Houses").get_children()
    var wanted_furniture_ids = []
    for house in house_nodes:
        if house.level > world_node.level:
            continue
        for wanted_id in house.wanted_furniture_ids:
            wanted_furniture_ids.append(wanted_id)
    if wanted_furniture_ids.size() > 0:
        return util.rand_from_array(wanted_furniture_ids)
    return -1
    
func get_spawn_position():
    if spawns_node.get_child_count() <= 0:
        return null
        
    var spawn_points = spawns_node.get_node(str(world_node.score)).get_children()
    
    var random_idx = floor(rand_range(0, spawn_points.size()))
    var spawn_position = spawn_points[random_idx]
    return spawn_position.global_position