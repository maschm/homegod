# Global Game Jam

For screen shots, download and further information please see the GGJ website
https://globalgamejam.org/2019/games/home-god

# License

We release all our source code and own sound assets to the public domain.

# Assets
* the character was also randomly pulled from the internet
* win sound 1.wav by remaxim form opengameart.org
* god image from http://www.bbcamerica.com/anglophenia/2015/10/watch-terry-gilliams-grumpy-commentary-for-lost-python-animations
