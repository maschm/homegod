extends Sprite

var old_size = Vector2()

func _ready():
    old_size = get_viewport().get_visible_rect().size
    get_viewport().connect("size_changed", self, "size_changed")
    
func size_changed():
    print("size changed")
    var new_size = get_viewport().get_visible_rect().size
    scale = new_size / old_size * Vector2(0.8, 0.8)
    pass