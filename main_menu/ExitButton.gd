extends Button

export(NodePath) var troll_god

func _input(event):
    if event.is_action_pressed("ui_cancel"):
        get_tree().quit()

func _on_ExitButton_pressed():
    get_tree().quit()
