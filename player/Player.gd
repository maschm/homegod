extends KinematicBody2D

enum Facing { Left, Right }

# Declare member variables here. Examples:
var vel = Vector2()
var last_jump_triggered = false
var jump_started = false # Jump started this frame
var on_left_wall = false
var left_wall_slide = false
var on_right_wall = false
var right_wall_slide = false
var inventory = null
var facing = Facing.Right
var cast_started = false
var placement_started = false
var was_on_floor = false;
var global_starting_position = null
var is_running = false
var was_running = false

const GRAVITY = 1200
const WALK_ACCELERATION = 3000
const BRAKE_ACCELERATION = 4000
const MAX_SPEED = 400
const JUMP_SPEED = -620
const ATTACK_SPEED = 280
const AIR_CONTROL_FACTOR = 0.4
const WALL_SLIDE_SPEED = 140

var WALL_JUMP_DIRECTION_LEFT = Vector2(-1.0, -2.0).normalized()
var WALL_JUMP_DIRECTION_RIGHT = Vector2(-WALL_JUMP_DIRECTION_LEFT.x, WALL_JUMP_DIRECTION_LEFT.y)

var initialized = false

# Called when the node enters the scene tree for the first time.
func _ready():
    var sprite = $AnimatedSprite
    sprite.animation = "idle"
    global_starting_position = global_position

func get_control_factor():
    if !is_on_floor() && !on_left_wall && !on_right_wall:
        return AIR_CONTROL_FACTOR;
    return 1.0


func animation_finished(sprite):
    return sprite.frame+1 == sprite.frames.get_frame_count(sprite.animation)

    
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    if global_position.y > 4750:
        global_position = global_starting_position
        vel = Vector2(0.0, 0.0)
        return
        
    var LEFT = Input.is_action_pressed("ui_left")
    var RIGHT = Input.is_action_pressed("ui_right")
    var JUMP = Input.is_action_pressed("ui_up")
    var TOGGLE_ITEM = Input.is_action_just_pressed("ui_accept")
    
    is_running = is_on_floor() and abs(vel.x) > 0
    cast_started = false
    placement_started = false
    
    if TOGGLE_ITEM:
        toggle_item()
        
    var wants_jump = JUMP && !last_jump_triggered
    if !JUMP:
        last_jump_triggered = false

    # set (overwriteable) vel, anim & flip
    if RIGHT:
        vel.x += WALK_ACCELERATION * get_control_factor() * delta
    elif LEFT:
        vel.x -= WALK_ACCELERATION * get_control_factor() * delta
    elif is_on_floor():
        var brake = min(BRAKE_ACCELERATION * delta, abs(vel.x))
        if vel.x > 0:
            vel.x -= brake
        else:
            vel.x += brake
            
    if is_on_ceiling() and vel.y < 0:
        vel.y = 0.0
        
    left_wall_slide = !is_on_floor() && on_left_wall && LEFT
    right_wall_slide = !is_on_floor() && on_right_wall && RIGHT
    
    # start jump?
    if is_on_floor():
        if wants_jump:
            vel.y = JUMP_SPEED
            last_jump_triggered = true
        else:
            vel.y = vel.y + GRAVITY * delta
    else: 
        if right_wall_slide:
            vel.y = min(WALL_SLIDE_SPEED, vel.y + GRAVITY * delta)
            if wants_jump:
                vel = WALL_JUMP_DIRECTION_LEFT*abs(JUMP_SPEED)
                last_jump_triggered = true
        elif left_wall_slide:
            vel.y = min(WALL_SLIDE_SPEED, vel.y + GRAVITY * delta)
            if wants_jump:
                vel = WALL_JUMP_DIRECTION_RIGHT*abs(JUMP_SPEED)
                last_jump_triggered = true
        else:
            vel.y = vel.y + GRAVITY * delta

    vel.x = clamp(vel.x, -MAX_SPEED, MAX_SPEED)
    
    jump_started = last_jump_triggered and wants_jump
    update_facing()
    update_item()
    update_animation()
    update_sound()
    var last_vel = vel
    was_on_floor = is_on_floor()
    was_running = is_running
    vel = move_and_slide(vel, Vector2(0, -1))
    
    on_left_wall = false
    on_right_wall = false
    
    var RAY_CAST_LENGTH = 15.0
    on_left_wall = slidable_in_direction(Vector2(-RAY_CAST_LENGTH, 0.0))
    on_right_wall = slidable_in_direction(Vector2(RAY_CAST_LENGTH, 0.0))
        
    move_grab_area_to_correct_side()
    $Camera2D.add_trauma(vel - last_vel)

func _process(delta):
    if not initialized:
        initialized = true
        var target = get_node("/root/World/Map/map/Player")
        get_parent().remove_child(self)
        target.add_child(self)

        
func slidable_in_direction(direction : Vector2):
    var space_state = get_world_2d().direct_space_state
    var result = space_state.intersect_ray(global_position, global_position + direction, [self])
        
    if result && (!result.collider || result.collider.name != "BarrierBody"):
        return true
    return false


func update_facing():
    if vel.x < 0:
        facing = Facing.Left
    elif vel.x > 0:
        facing = Facing.Right
    

func move_grab_area_to_correct_side():
    if vel.x < 0:
        $GrabArea.scale.x = -1
    elif vel.x > 0:
        $GrabArea.scale.x = 1
        

func update_animation():
    var sprite = $AnimatedSprite
    if vel.x < 0:
        sprite.flip_h = true
    elif vel.x > 0:
        sprite.flip_h = false
    
    if cast_started:
        sprite.animation = "cast"
    elif sprite.animation == "cast" && !animation_finished(sprite):
        pass          
    elif jump_started:
        sprite.animation = "start_jump"
    elif !is_on_floor():
        if left_wall_slide:
            sprite.animation = "wall_slide"
            sprite.flip_h = true
        elif right_wall_slide:
            sprite.animation = "wall_slide"
            sprite.flip_h = false
        elif sprite.animation == "start_jump":
            if animation_finished(sprite):
                sprite.animation = "in_jump";
        else:
            sprite.animation = "in_jump";
    elif abs(vel.x) > 0:
        sprite.animation = "run"
    else:
        sprite.animation = "idle"
    if !was_on_floor and is_on_floor():
        var anim = $LandingAnimation
        anim.visible = true
        anim.frame = 0
        anim.play()
        

func update_sound():
    if jump_started:
        $Sound_Jump.play()
        
    if cast_started:
        $Sound_Cast.play()
        
    if !was_on_floor and is_on_floor():
        $Sound_Land.play()
        
    if is_running and !was_running:
        $Sound_Running.start_cycle()
    elif !is_running and was_running:
        $Sound_Running.stop_cycle()
    
    var should_be_playing = left_wall_slide or right_wall_slide
    if $Sound_WallSlide.playing != should_be_playing:
        $Sound_WallSlide.playing = should_be_playing
        
    if placement_started:
        $Sound_Placement.play()
    

func update_item():
    if !inventory:
        return
    inventory.set_target_position(global_position, true)    


func get_target_pickup():
    for area in $GrabArea.get_overlapping_areas():
        if area.name == "Pickup":
            return area.get_parent()
    return null
            
    
func try_pick_up():
    var target = get_target_pickup()
    if target == null:
        return
    pick_up(target)
    cast_started = true
    
func toggle_item():
    if is_inventory_empty():
        try_pick_up()
        return
    put_down()

func pick_up(item):
    inventory = item
    item.get_node("Pickup/CollisionShape2D").disabled = true
    item.set_collision_layer_bit(0, false)
    inventory.set_collision_mask_bit(0, false)

func grab_space_free():
    for body in $GrabArea.get_overlapping_bodies():
        if body.get_parent() != self:
            return false
    return true

func put_down():
    if !is_on_floor():
        return
    
    if grab_space_free():
        return
    
    $GrabArea/Shape/Particles2D.emitting = true
    inventory.set_target_position($GrabArea/Shape.global_position, false)
    inventory.get_node("Pickup/CollisionShape2D").disabled = false
    inventory.set_collision_layer_bit(0, true)
    inventory.set_collision_mask_bit(0, true)
    inventory = null;
    placement_started = true
    
    
func is_inventory_empty():
    return inventory == null;



func _on_LandingAnimation_animation_finished():
    $LandingAnimation.visible = false
