extends AudioStreamPlayer2D

var step_files = []
var cycling = false

# Called when the node enters the scene tree for the first time.
func _ready():
    for i in range(1, 4):
        var filename = "res://assets/sound/steps/step_grass_0"+str(i)+".wav"
        step_files.append(load(filename))
    select_next()

func start_cycle():
    cycling = true
    play()
    
func stop_cycle():
    cycling = false
    
func select_next():
    var index = randi() % step_files.size()
    stream = step_files[index]

func _on_Sound_Running_finished():
    if !cycling:
        return
    select_next()
    play()
