extends Camera2D

# Declare member variables here. Examples:
var trauma = 0.0
var TRAUMA_INCREASE = 0.6
var TRAUMA_DECREASE = 1.0
var TRAUMA_MOVE_FACTOR = Vector2(10.0, 40.0)
var TRAUMA_ROTATE_FACTOR = 40.0
var time = 0.0

# see https://docs.godotengine.org/en/latest/classes/class_opensimplexnoise.html?highlight=noise
var noise = OpenSimplexNoise.new()

# Called when the node enters the scene tree for the first time.
func _ready():
    noise.seed = randi()
    noise.octaves = 1
    noise.period = 10
    noise.persistence = 0.8

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    time += delta
    var shake = trauma * trauma
    var nx = noise.get_noise_2d(time*20, 1337.0)
    var ny = noise.get_noise_2d(time*20, 677.0)
    var nr = noise.get_noise_2d(time*20, 4567.0)
    offset.x = TRAUMA_MOVE_FACTOR.x * shake * nx
    offset.y = TRAUMA_MOVE_FACTOR.y * shake * ny
    rotation_degrees = nr * shake * TRAUMA_ROTATE_FACTOR
    trauma = max(0.0, trauma - delta * TRAUMA_DECREASE)


func add_trauma(t: Vector2):
    if abs(t.y) > 25:
        trauma += t.length() * TRAUMA_INCREASE
        if trauma > 1.0:
            trauma = 1.0