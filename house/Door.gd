extends CollisionShape2D

onready var player = get_node("/root/World/Player")

func _ready():
    pass # Replace with function body.

func _process(delta):
    var dist = (global_position - player.global_position).length()
    if dist < 50:
        disabled = true
    else:
        disabled = false
    pass
