extends Node2D

const Furniture = preload("res://furniture/Furniture.gd")

onready var path = get_node("WalkingPath")
onready var furniture_points = get_node("Points").get_children()
onready var drop_area = get_node("DropArea")
onready var world = get_node("/root/World")
onready var inhabitant = $Inhabitant

export var wanted_furniture_ids = [0,1,2,3,4,5,6,7,8]
export var level = 0

func add_furniture(furniture):
    for point in furniture_points:
        if !point.is_occupied && point.type == furniture.type:
            if !wanted_furniture_ids.has(furniture.furniture_id):
                inhabitant.react_to_wrong_furniture()
                return false
            point.pass_furniture(furniture)
            inhabitant.react_to_new_furniture(point)
            util.remove_first_from_array(wanted_furniture_ids, furniture.furniture_id)
            world.mission_completed()
            return true
    return false

func _physics_process(delta):
    for area in drop_area.get_overlapping_areas():
        var furniture = area.get_parent()
        if furniture is Furniture and not furniture.is_controlled():
            add_furniture(furniture)