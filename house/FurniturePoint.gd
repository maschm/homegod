extends Node2D

const Furniture = preload("res://furniture/Furniture.gd")

var is_occupied = false
export(Furniture.Type) var type = Furniture.Type.GROUND

func _ready():
    $Sprite.visible = false
    
func pass_furniture(furniture: Node2D):
    if is_occupied:
        print("error: passed furniture to occupied furniture point")
        return
    is_occupied = true
    furniture.place_in_house()
    furniture.get_parent().remove_child(furniture)
    var old_global_pos = furniture.global_position
    add_child(furniture)
    furniture.global_position = old_global_pos
    furniture.set_target_position(self.global_position, false)