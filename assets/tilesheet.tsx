<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="tilesheet" tilewidth="64" tileheight="64" tilecount="220" columns="10">
 <image source="tilesheet.png" trans="6f6d51" width="640" height="1408"/>
 <terraintypes>
  <terrain name="Floor" tile="-1"/>
  <terrain name="FloorHidden" tile="0"/>
 </terraintypes>
 <tile id="0" terrain=",,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="1" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="2" terrain=",,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="5" terrain="0,0,0,">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="6" terrain="0,0,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="10" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="11" terrain="0,0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="12" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="15" terrain="0,,0,0">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="16" terrain=",0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="20" terrain=",0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="21" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="22" terrain="0,,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="26">
  <objectgroup draworder="index">
   <object id="1" x="32.2565" y="0.327477">
    <polygon points="-0.141375,-0.282749 0,3.76598 31.4378,3.92972 31.7435,-0.327477"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="28">
  <objectgroup draworder="index">
   <object id="1" x="32.0927" y="0.491215">
    <polygon points="-0.0706874,-0.424124 0,4.2572 -31.7653,4.58468 -32.0927,-0.491215"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="31">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="31.929" height="47.8116"/>
  </objectgroup>
 </tile>
 <tile id="32">
  <objectgroup draworder="index">
   <object id="1" x="32.5828" y="0" width="31.4172" height="47.8116"/>
  </objectgroup>
 </tile>
 <tile id="35">
  <objectgroup draworder="index">
   <object id="1" x="0" y="64">
    <polygon points="0,0 64,-64 63.6943,-44.3514 20.631,-0.633209"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="36">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0.163738,5.23963 63.7632,6.22206 64,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="37">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0.327477,4.09346 63.858,4.2572 64,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="38">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 64,0 63.5305,5.07589 0.327477,4.2572"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="39">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 64,64 44.5369,63.858 0.327477,20.631"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="44">
  <objectgroup draworder="index">
   <object id="1" x="64" y="64">
    <polygon points="0,0 -0.469471,-27.9775 -54.3394,-0.633209"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="45">
  <objectgroup draworder="index">
   <object id="1" x="0" y="64">
    <polygon points="0,0 55.6711,-0.305732 0.491215,-27.6501"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="52">
  <objectgroup draworder="index">
   <object id="1" x="64" y="64">
    <polygon points="0,0 -0.296393,-28.4824 -55.5279,-0.622242"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="53">
  <objectgroup draworder="index">
   <object id="1" x="0" y="64">
    <polygon points="0,0 9.49683,-0.141994 63.5305,-27.8138 63.6943,-60.234 0.327477,-28.4688"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="54">
  <objectgroup draworder="index">
   <object id="1" x="0.654954" y="3.60225">
    <polygon points="0,0 5.89459,-3.11103 63.345,-3.60225 62.8756,0.654954 -0.163738,32.2565"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="55">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0.654954,4.58468 63.3668,35.695 63.6943,3.43851 57.1447,0.327477"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="56">
  <objectgroup draworder="index">
   <object id="1" x="0.163738" y="3.76598">
    <polygon points="0,0 63.2031,31.6015 63.8363,60.234 54.5249,59.7645 0.327477,32.2565"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="57">
  <objectgroup draworder="index">
   <object id="1" x="0.327477" y="36.1862">
    <polygon points="0,0 -0.327477,27.8138 55.9986,27.9993"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="60">
  <objectgroup draworder="index">
   <object id="1" x="64" y="64">
    <polygon points="0,0 0.0294568,-28.6453 -55.365,-0.622242"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index">
   <object id="2" x="64.0295" y="3.74727">
    <polygon points="-0.488774,0.162925 -63.2148,31.2816 -63.3778,59.7934 -54.9057,59.9563 -0.651699,32.0962"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index">
   <object id="1" x="0.651699" y="3.09557">
    <polygon points="0,0 5.86529,-2.6068 63.3483,-3.09557 63.2148,1.14047 -0.162925,33.2367"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="67">
  <objectgroup draworder="index">
   <object id="1" x="0" y="4.42094">
    <polygon points="0,0 63.5305,31.7653 63.3668,-1.30991 57.6359,-3.92972 0.491215,-4.2572"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="68">
  <objectgroup draworder="index">
   <object id="1" x="0.491215" y="4.42094">
    <polygon points="0,0 62.8756,31.7653 63.5088,59.5791 54.3612,59.2733 0,32.0927"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="69">
  <objectgroup draworder="index">
   <object id="1" x="0.818692" y="36.5137">
    <polygon points="0,0 54.6887,27.0168 -0.818692,27.4863"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="70">
  <objectgroup draworder="index">
   <object id="2" x="0.327477" y="2.78355">
    <polygon points="0,0 6.05832,-2.78355 63.6725,-2.78355 63.5305,20.1398 0.163738,19.9761"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="71">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32.1062,0.214041 31.8856,64.1189 0,64"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index">
   <object id="1" x="32.2591" y="0" width="31.7409" height="64"/>
  </objectgroup>
 </tile>
 <tile id="79">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 0.491215,23.0871 63.5305,22.4322 63.6943,3.27477 57.3085,0.163738"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="81">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="32.0667" height="64"/>
  </objectgroup>
 </tile>
 <tile id="88">
  <objectgroup draworder="index">
   <object id="1" x="32.0962" y="0" width="31.9038" height="64"/>
  </objectgroup>
 </tile>
 <tile id="91">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="32.0667" height="64"/>
  </objectgroup>
 </tile>
 <tile id="98">
  <objectgroup draworder="index">
   <object id="1" x="32.2591" y="0" width="31.7409" height="64"/>
  </objectgroup>
 </tile>
 <tile id="150" terrain=",,,1"/>
 <tile id="151" terrain=",,1,1"/>
 <tile id="152" terrain=",,1,"/>
 <tile id="155" terrain="1,1,1,"/>
 <tile id="156" terrain="1,1,,1"/>
 <tile id="160" terrain=",1,,1"/>
 <tile id="161" terrain="1,1,1,1"/>
 <tile id="162" terrain="1,,1,"/>
 <tile id="165" terrain="1,,1,1"/>
 <tile id="166" terrain=",1,1,1"/>
 <tile id="170" terrain=",1,,"/>
 <tile id="171" terrain="1,1,,"/>
 <tile id="172" terrain="1,,,"/>
 <tile id="203">
  <objectgroup draworder="index">
   <object id="1" x="4.08391" y="63.838">
    <polygon points="0,0 8.66936,-4.87204 40.8391,-4.87204 46.8575,-3.86897 54.8104,-3.79732 59.969,-1.79119 59.969,0.28659"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="204">
  <objectgroup draworder="index">
   <object id="1" x="0" y="64">
    <polygon points="0,0 0.0982811,-1.98465 5.11062,-1.98465 12.7765,-3.95027 19.7545,-4.04855 25.7496,-1.98465 29.0912,-0.903557 35.0863,-0.903557 39.1159,-1.88637 45.111,-3.85199 52.9735,-3.85199 58.8704,-2.08293 64.0793,-2.18121 64,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="205">
  <objectgroup draworder="index">
   <object id="1" x="59.8532" y="64.0793">
    <polygon points="0,0 -8.74701,-4.91405 -40.7866,-4.91405 -46.6835,-3.93124 -54.7426,-3.93124 -59.5583,-2.0639 -59.8532,-0.0792539"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="214">
  <objectgroup draworder="index">
   <object id="1" x="0" y="3.07222">
    <polygon points="0,0 12.0178,-2.07827 19.9695,-2.07827 30.9933,2.80115 32.8909,2.80115 44.0051,-2.16863 52.1375,-1.98791 63.8842,-0.180719 64,-3.07222 0,-3.07222"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
