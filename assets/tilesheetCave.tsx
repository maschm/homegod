<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="tilesheetCave" tilewidth="64" tileheight="64" tilecount="200" columns="10">
 <image source="tilesheetCave.png" width="640" height="1280"/>
 <terraintypes>
  <terrain name="Walls" tile="0"/>
 </terraintypes>
 <tile id="0" terrain=",,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="1" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="2" terrain=",,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="5" terrain="0,0,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="6" terrain="0,0,,0"/>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="10" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="11" terrain="0,0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="12" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="15" terrain="0,,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="16" terrain=",0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="20" terrain=",0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="21" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="22" terrain="0,,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="190">
  <objectgroup draworder="index">
   <object id="1" x="9.39776" y="67.0896">
    <polygon points="0,0 -3.13259,-15.1408 16.4461,-31.3259 26.888,-25.0607 32.8922,-28.7154 40.2015,-23.2334 46.2057,-15.1408 48.5551,-1.30524"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="191">
  <objectgroup draworder="index">
   <object id="1" x="1.82734" y="64.4791">
    <polygon points="0,0 1.56629,-17.7513 10.1809,-36.5468 14.0966,-31.5869 20.3618,-39.4184 29.7596,-26.888 34.1974,-15.1408 45.4225,-14.6187 52.2098,-6.52622 53.7761,2.08839"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="192">
  <objectgroup draworder="index">
   <object id="1" x="4.69888" y="67.0896">
    <polygon points="0,0 -2.61049,-9.91986 4.17678,-19.5787 6.52622,-19.0566 5.74308,-27.9322 11.4862,-45.4225 19.3176,-61.3465 26.627,-50.3824 31.5869,-58.2139 33.6753,-52.2098 39.4184,-37.8521 40.9847,-29.2375 44.1173,-28.9764 43.0731,-3.91573"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
